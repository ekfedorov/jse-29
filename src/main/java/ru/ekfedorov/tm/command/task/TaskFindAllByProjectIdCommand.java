package ru.ekfedorov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.service.IProjectTaskService;
import ru.ekfedorov.tm.command.AbstractTaskCommand;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.model.Task;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskFindAllByProjectIdCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Find al lby project id.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "find-all-by-project-id";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (serviceLocator == null) throw new NullObjectException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[TASK LIST BY PROJECT]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final IProjectTaskService projectTaskService = serviceLocator.getProjectTaskService();
        @NotNull final List<Task> tasks = projectTaskService.findAllByProjectId(userId, id);
        if (tasks.isEmpty()) {
            System.out.println("--- There are no tasks ---");
            return;
        }
        int index = 1;
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

}

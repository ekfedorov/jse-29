package ru.ekfedorov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.service.ITaskService;
import ru.ekfedorov.tm.command.AbstractTaskCommand;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.exception.system.NullTaskException;
import ru.ekfedorov.tm.model.Task;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public final class TaskChangeStatusByNameCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Change task status by name.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "change-task-status-by-name";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (serviceLocator == null) throw new NullObjectException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final Optional<Task> task = taskService.findOneByName(userId, name);
        if (!task.isPresent()) throw new NullTaskException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusId = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusId);
        @NotNull final Optional<Task> taskUpdated = taskService.changeStatusByName(userId, name, status);
        if (!taskUpdated.isPresent()) throw new NullTaskException();
    }

}

package ru.ekfedorov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.IBusinessService;
import ru.ekfedorov.tm.model.Task;

public interface ITaskService extends IBusinessService<Task> {

    @NotNull
    Task add(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

}
